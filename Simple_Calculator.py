#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed May  9 14:21:28 2018

@author: yoobin
"""

from PyQt5.QtWidgets import QApplication, QWidget, QLineEdit, QLabel, QVBoxLayout
from PyQt5.QtCore import Qt
from sympy import *
import re
#from IPython.display import display
#init_printing(use_latex='mathjax')

class Calculator(QWidget):
    def __init__(self, parent = None):
        super(Calculator, self).__init__(parent)
        self.setWindowFlags(Qt.WindowStaysOnTopHint)
        self.initUI()
        self.textbox.returnPressed.connect(self.calculate)    
        
        # capture 1: command
        # capture 2: expression
        # captrue 3: variable
        self.regex = r'(\w*)\(\s*(.*)\s*,\s*(\D)\s*\)'       

    
    def initUI(self):
        self.setWindowTitle("Math GUI")
        self.setGeometry(300, 300, 400, 150)
        
        # The text box 
        self.textbox = QLineEdit(self)

        # Labels
        self.label1 = QLabel(self)
        self.label1.setText("Enter math command:")
        self.label2 = QLabel(self)
        self.label2.setText("Result:")
        self.label3 = QLabel(self)

        layout = QVBoxLayout(self)
        layout.addWidget(self.label1)
        layout.addWidget(self.textbox)
        layout.addWidget(self.label2)
        layout.addWidget(self.label3)
                
    def calculate(self):
        input = self.textbox.text()
        find = re.search(self.regex, input)
        try:      # if command is found
            command = find.group(1)
            expression = find.group(2)
            expression = re.sub('\^','**',expression)       # change ^ to **
            expression = re.sub('=','-',expression)         # change = to -
            variable = symbols(find.group(3))
            if command in ['diff','differentiate','derivative']:
                output = str(diff(expression, variable))
                self.label3.setText(re.sub('\*\*','^',output))      # change ** to ^     
            elif command in ['int','integral','integrate']:
                output = str(integrate(expression, variable))
                self.label3.setText(re.sub('\*\*','^',output))      # change ** to ^  
            elif command in ['solve','sol','solution']:
                output = str(solve(expression, variable))
                self.label3.setText(re.sub('\*\*','^',output))      # change ** to ^ 
            else:
                self.label3.setText("INVALID COMMAND")
        except:   # if no command
            try:
                output = str(sympify(input))
                self.label3.setText(re.sub('\*\*','^',output))      # change ** to ^
            except:    # if expression is wrong
                self.label3.setText("INVALID EXPRESSION")

def main():
    app = QApplication([])
    calculator = Calculator()
    calculator.show()
    app.exec_()

if __name__ == '__main__':
    main()